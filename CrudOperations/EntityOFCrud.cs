﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;


namespace CrudOperations
{
    public class EntityOfCrud
    {
        //ConnectionToDatabase conobj = new ConnectionToDatabase();
        //inserting values
        public void InsertStudentDataIntoStudenInfoTable(ConnectionToDatabase conobj)
        {

            
            try
            {

              
                    Console.WriteLine("enter student id");
                    String id1 = Console.ReadLine();

                    Console.WriteLine("enter student name");
                    String name1 = Console.ReadLine();
            
                           Console.WriteLine("enter student roll");
                        int roll1 = int.Parse(Console.ReadLine());
                   

                    Console.WriteLine("enter student email id");
                    String email1 = Console.ReadLine();



                    SqlCommand cmd = new SqlCommand($"insert into  StudentInfo values('{id1}','{name1}',{roll1},'{email1}');", conobj.GetConnection());
                    conobj.OpenConnection();
                    cmd.ExecuteNonQuery();
                    conobj.CloseConnection();

                    Console.WriteLine("\nYOUR DETAILS ARE INSERTED SUCCESSFULLY !!");


                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //Diplay Student Data From StudentInfo Table
        public void DiplayStudentDataFromStudentInfoTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id to display details");
            String id1 = Console.ReadLine();
            SqlCommand cmd = new SqlCommand($"select *from StudentInfo where id='{id1}';", conobj.GetConnection());
            conobj.OpenConnection();
            SqlDataReader dr = cmd.ExecuteReader();
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("ID \t NAME \t  ROLL \t EMAIL");
            Console.WriteLine("----------------------------------------");
            while (dr.Read())
            {

                Console.Write(dr["id"]+ "\t");
                Console.Write(dr["name"] + "\t");
                Console.Write(dr["roll"] + "\t");
                Console.Write(dr["email"] + "\n");
               
            }
            conobj.CloseConnection();
        }

        //displaying total data in student info table

        public void DiplayTotalDataInStudentInfoTable(ConnectionToDatabase conobj)
        {
            SqlCommand cmd = new SqlCommand("select *from StudentInfo;", conobj.GetConnection());
            conobj.OpenConnection();
            SqlDataReader dr = cmd.ExecuteReader();
            Console.WriteLine("\nID \t NAME \t  ROLL \t EMAIL");
            while (dr.Read())
            {

                Console.Write(dr["id"] + "\t");
                Console.Write(dr["name"] + "\t");
                Console.Write(dr["roll"] + "\t");
                Console.Write(dr["email"]+"\t");
                Console.WriteLine("\n");
            }
            conobj.CloseConnection();
        }

        ///Update StudentData In StudentInfo Table
        public void UpdateStudentDataInStudentInfoTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id ");
            String id1 = Console.ReadLine();

            Console.WriteLine("enter student details to update");

            Console.WriteLine("enter student name");
            String name1 = Console.ReadLine();

            Console.WriteLine("enter student roll");
            int roll1 = int.Parse(Console.ReadLine());

            Console.WriteLine("enter student email id");
            String email1 = Console.ReadLine();

            SqlCommand cmd = new SqlCommand($"update  StudentInfo set name='{name1}',roll={roll1},email='{email1}' where id='{id1}';", conobj.GetConnection());

            conobj.OpenConnection();
            cmd.ExecuteNonQuery();
            conobj.CloseConnection();
        }

        //delete student record from studentinfo table
        public void DeleteStudentDataFromTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id to delete student information");
            string id1 = Console.ReadLine();

            SqlCommand cmd = new SqlCommand($"delete from   StudentInfo where id='{id1}';", conobj.GetConnection());
            conobj.OpenConnection();
            cmd.ExecuteNonQuery();
            conobj.CloseConnection();
        }

        public int  Exit()
        {
            return 6;
        }
    }
}
