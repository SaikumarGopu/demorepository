﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace CrudOperations
{
    public class Entity
    {

        //inserting values
        public  void InsertStudentDataIntoStudenInfoTable(ConnectionToDatabase conobj)
        {
            
            Console.WriteLine("enter how many  student details u want to enter");
            int i = int.Parse(Console.ReadLine());
            try
            {

                for (int j = 0; j < i; j++)
                {
                    Console.WriteLine("enter student id");
                    String id1 = Console.ReadLine();

                    Console.WriteLine("enter student name");
                    String name1 = Console.ReadLine();
                    try
                    {
                        Console.WriteLine("enter student roll");
                        int roll1 = int.Parse(Console.ReadLine());
                    }
                    catch ()
                    {
                        throw new CustomException("invalid roll number");
                    }

                    Console.WriteLine("enter student email id");
                    String email1 = Console.ReadLine();



                    SqlCommand cmd = new SqlCommand($"insert into  StudentInfo values('{id1}','{name1}',{roll1},'{email1}');", conobj.GetConnection());
                    conobj.OpenConnection();
                    cmd.ExecuteNonQuery();
                    conobj.CloseConnection();

                    Console.WriteLine("\n Your details were stored");


                }
            }
            catch(CustomException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //Diplay Student Data From StudentInfo Table
        public void DiplayStudentDataFromStudentInfoTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id to display details");
            String id1 = Console.ReadLine();
            SqlCommand cmd = new SqlCommand($"select *from StudentInfo where id='{id1}';", conobj.GetConnection());
            conobj.OpenConnection();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                Console.WriteLine(dr["id"]);
                Console.WriteLine(dr["name"]);
                Console.WriteLine(dr["roll"]);
                Console.WriteLine(dr["email"]);
            }
            conobj.CloseConnection();
        }

        //displaying total data in student info table

        public  void DiplayTotalDataInStudentInfoTable(ConnectionToDatabase conobj)
        {
            SqlCommand cmd = new SqlCommand("select *from StudentInfo;", conobj.GetConnection());
            conobj.OpenConnection();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                Console.WriteLine(dr["id"]);
                Console.WriteLine(dr["name"]);
                Console.WriteLine(dr["roll"]);
                Console.WriteLine(dr["email"]);
            }
            conobj.CloseConnection();
        }

        ///Update StudentData In StudentInfo Table
        public void UpdateStudentDataInStudentInfoTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id to update ");
            String id1 = Console.ReadLine();

            Console.WriteLine("enter student name");
            String name1 = Console.ReadLine();

            Console.WriteLine("enter student roll");
            int roll1 = int.Parse(Console.ReadLine());

            Console.WriteLine("enter student email id");
            String email1 = Console.ReadLine();

            SqlCommand cmd = new SqlCommand($"update  StudentInfo set name='{name1}',roll={roll1},email='{email1}' where id='{id1}';", conobj.GetConnection());

            conobj.OpenConnection();
            cmd.ExecuteNonQuery();
            conobj.CloseConnection();
        }

        //delete student record from studentinfo table
        public  void DeleteStudentDataFromTable(ConnectionToDatabase conobj)
        {
            Console.WriteLine("enter student id to delete student information");
            string id1 = Console.ReadLine();

            SqlCommand cmd = new SqlCommand($"delete from   StudentInfo where id='{id1}';", conobj.GetConnection());
            conobj.OpenConnection();
            cmd.ExecuteNonQuery();
            conobj.CloseConnection();
        }


    }
}
